**Memory of material (Shape memory effect)**

|Discipline|Physics|
| :- | :- |
|Theme|Material Science and Engineering|
|Module|Memory of material (Shape memory effect)|

**About the module:**

Memory of material (Shape memory effect)

**Target Audience:**

Class 8 to 12

**Course alignment:**

Physics, Material Science 

**Boards mapped:**

CBSE, ISCE, State Boards

**Language:**

English


|Name of Developer/ principal investigator|Dr. Satanand Mishra|
| :- | :- |
|Institute|CSIR-AMPRI|
|Email. ID|snmishra07@gmail.com|
|Department|CARS & GM|

Contributor List

|Sr. No.|Name |Department|Institute|Email id|
| :- | :- | :- | :- | :- |
|1|Dr. S. A. R. Hashmi|<p>Intelligent Materials and Advanced Processes</p><p></p>|CSIR-AMPRI, Bhopal|sarhashmi@ampri.res.in|
|2|Dr. Gaurav K Gupta|<p>Intelligent Materials and Advanced Processes</p><p></p>|CSIR-AMPRI, Bhopal|gauravgupta@ampri.res.in|



